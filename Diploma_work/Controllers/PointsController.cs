﻿using CsvHelper;
using Diploma_work.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.VisualBasic.FileIO;

namespace Diploma_work.Controllers
{
    public class PointsController : Controller
    {
        [HttpPost]
        [ActionName("GetPoint")]
        [Route("[controller]/{searchRequest}")]
        public ActionResult GetPoint(string searchRequest)
        {
            string url = "https://nominatim.openstreetmap.org/search?q={0}&format=json&polygon=0&addressdetails=1";
            HttpClient client = new HttpClient();
            string cityName = searchRequest.Replace(' ', '+');

            client.DefaultRequestHeaders.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            var response = client.GetAsync(string.Format(url, cityName)).Result;
            HttpContent content = response.Content;

            Task<String> result = content.ReadAsStringAsync();
            List<RootObject> rootList = new List<RootObject>();

            rootList = JsonConvert.DeserializeObject<List<RootObject>>(result.Result);

            List<SearchResult> searchResultList = new List<SearchResult>();

            SearchResult searchResult = new SearchResult();

            foreach (var item in rootList)
            {
                searchResultList.Add(
                    new SearchResult
                    {
                        place_id = item.place_id,
                        osm_id = item.osm_id,
                        osm_type = item.osm_type,
                        lat = item.lat,
                        lon = item.lon,
                        display_name = item.display_name,
                        type = item.type,
                        importance = item.importance,
                        icon = item.icon,
                        city = item.address.city,
                        state = item.address.state,
                        postcode = item.address.postcode,
                        country = item.address.country,
                        country_code = item.address.country_code
                    });
            }
            return Json(searchResultList);
        }

        [HttpPost]
        [ActionName("GetPoints")]
        //[Route("[controller]/{searchRequest}")]
        public ActionResult GetPointsCSV(HttpPostedFileBase file)
        {
            List<CSVModel> CSVList = new List<CSVModel>();
            
            var fileName = Path.GetFileName(file.FileName);

            using (TextFieldParser csvParser = new TextFieldParser(fileName))
            {
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                csvParser.ReadLine();

                while (!csvParser.EndOfData)
                {
                    // Read current line fields, pointer moves to the next line.
                    string[] fields = csvParser.ReadFields();

                    CSVModel Record = new CSVModel();

                    Record.id = Int32.Parse(fields[0]);
                    Record.name = fields[1];
                    Record.latitude = Double.Parse(fields[2].Replace('.', ','));
                    Record.longitude = Double.Parse(fields[3].Replace('.', ','));

                    CSVList.Add(Record);
                }

                //CsvReader csv = new CsvReader(reader);
                //csv.Configuration.Delimiter = ",";
                //csv.Configuration.MissingFieldFound = null;
                //while (csv.Read())
                //{
                //    CSVModel Record = csv.GetRecord<CSVModel>();
                //    CSVList.Add(Record);
                //}
            }

            return Json(CSVList);
        }
    }

    public class Address
    {
        public string city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        public string county { get; set; }
        public string station { get; set; }
        public string road { get; set; }
        public string suburb { get; set; }
        public string hotel { get; set; }
        public string house_number { get; set; }
        public string supermarket { get; set; }
        public string cafe { get; set; }
        public string information { get; set; }
        public string water { get; set; }
        public string village { get; set; }
        public string address29 { get; set; }
        public string guest_house { get; set; }
    }

    public class RootObject
    {
        public string place_id { get; set; }
        public string licence { get; set; }
        public string osm_type { get; set; }
        public string osm_id { get; set; }
        public List<string> boundingbox { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string display_name { get; set; }
        public string @class { get; set; }
        public string type { get; set; }
        public double importance { get; set; }
        public string icon { get; set; }
        public Address address { get; set; }
    }

    public class SearchResult
    {
        public string place_id { get; set; }
        public string licence { get; set; }
        public string osm_type { get; set; }
        public string osm_id { get; set; }
        //public List<string> boundingbox { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string display_name { get; set; }
        public string type { get; set; }
        public double importance { get; set; }
        public string icon { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        public string county { get; set; }
        public string station { get; set; }
        public string road { get; set; }
        public string suburb { get; set; }
        public string hotel { get; set; }
        public string house_number { get; set; }
        public string supermarket { get; set; }
        public string cafe { get; set; }
        public string information { get; set; }
        public string water { get; set; }
        public string village { get; set; }
        public string address29 { get; set; }
        public string guest_house { get; set; }
    }
}