﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Diploma_work.Controllers
{
    [System.Web.Http.Route("api/[controller]")]
    public class FindPointController : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/[controller]/{searchRequest}")]
        public HttpContent GetPoint(string searchRequest)
        {
            string url = "https://geocode-maps.yandex.ru/1.x/?geocode={0}&format=json";

            HttpClient client = new HttpClient();
            string cityName = searchRequest.Replace(' ', '+');

            var response = client.GetAsync(string.Format(url, cityName)).Result;
            HttpContent content = response.Content;


            return content;
        }
    }
}
